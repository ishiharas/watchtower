import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nameTransformer'
})
export class NameTransformerPipe implements PipeTransform {

  transform(value: string): string {
    let newValue: Array<string> = [];
    let finalValue: Array<string> = [];

    if(value.includes("api")) {
      newValue = value.split("api", 1);
      finalValue = newValue[0].split("//", 2);
    } else {
      return "404 - NOT FOUND";
    }
    return finalValue[1][0].toUpperCase() + finalValue[1].substr(1).toLowerCase();
  }
}
