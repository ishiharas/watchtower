import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { DataSource } from '@angular/cdk/collections';
import { User } from '../../models/user.model';
import { Observable } from 'rxjs';
import {animate, state, style, transition, trigger} from '@angular/animations';


@Component({
  selector: 'app-usertable',
  templateUrl: './usertable.component.html',
  styleUrls: ['./usertable.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, void', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
      transition('expanded <=> void', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ],
})

export class UsertableComponent implements OnInit {

  dataSource = new UserDataSource(this.userService);
  displayedColumns = ['baseUrl', 'timeZone','lastFinishTime', 'success', 'sphere'];
  expandedElement: any;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }
}

export class UserDataSource extends DataSource<any> {
  constructor(private userService: UserService) {
    super();
  }
  connect(): Observable<Array<User[]>> {
      return this.userService.getAllUsers();
  }
  disconnect() {}
}
