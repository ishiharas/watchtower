export interface User {
    lastFinishTime: string;
    success: boolean;
    sphere: {
        name: string;
        baseUrl: string;
        timeZone: string;
    };
}