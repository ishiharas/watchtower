import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin, of, empty } from 'rxjs';
import { User } from '../models/user.model';
import { catchError } from 'rxjs/operators';

import * as linkList from '../services/linklist';
import * as emptyActuator from '../services/default-model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  private joinedRequests = [];
  private defaultModel: User[] = [emptyActuator.default];

  getUser(url): Observable<User[]> {
    return this.http.get<User[]>(url).pipe(catchError(error => {
      this.defaultModel[0].sphere.baseUrl = url; 
      return of(this.defaultModel);}));
}

  getAllUsers(): Observable<Array<User[]>> {
    const list = linkList.default.urls;
    list.forEach((url, index) => {
      let element = this.getUser(url);

      element.subscribe(res => {
        // console.log(res[0].lastFinishTime);
        let lastFinish = new Date(res[0].lastFinishTime).getTime();
        let actualDate = new Date().getTime();
        let diff = actualDate - lastFinish;

        let diff_days = Math.floor(diff/1000/60/60/24);
        let diff_hours = Math.floor(diff/1000/60/60);
        let diff_minutes = Math.floor(diff/1000/60);

        console.log(res[0].sphere.baseUrl);
        console.log("Passed Time: - " + diff_days + ":" + diff_hours + ":" + diff_minutes);

      });

      // getTimeDifference();
      this.joinedRequests.push(element);
     });
    return forkJoin(this.joinedRequests);
  }
}
